// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MoveActor.generated.h"

UCLASS()
class MOTORSPORT_API AMoveActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMoveActor();
	
	protected:

	UPROPERTY (VisibleAnywhere, BlueprintReadOnly, Category = "MoveActor")
		class USceneComponent* RootScene;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "MoveActor")
		class UStaticMeshComponent* Mesh;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "MoveActor")
	class USceneComponent* SceneForward;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "MoveActor")
		class USceneComponent* SceneRight;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "MoveActor")
		class USceneComponent* SceneLeft;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "MoveActor")
		class USceneComponent* SceneMiddleRight;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "MoveActor")
		class USceneComponent* SceneMiddleLeft;

		// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	

	
	// declare our float variables 
	UPROPERTY(EditAnywhere, Category = "MoveActor")
		float Speed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MoveActor")
		float Distance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MoveActor")
		float DistanceMiddleRight;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MoveActor")
		float DistanceMiddleLeft;
			
	//for rotation
	UPROPERTY(EditAnywhere, Category = "Movement")
		float PitchValue;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float YawValue;

	UPROPERTY(EditAnywhere, Category = "Movement")
		float RollValue;

	//Factor rotation

	UPROPERTY(EditAnywhere, Category = "Movement")
		float FactorRotation;

	UPROPERTY(EditAnywhere, Category = "Movement")
		float SmallFactorRotation;

	UPROPERTY(EditAnywhere, Category = "Movement")
		float LengthForwardTrace;

	//to adjust the movement

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool ForwardHand;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool RightHand;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool LeftHand;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool MiddleRight;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool MiddleLeft;
	
	//Enable ore disable line traces

	UPROPERTY(EditAnywhere, Category = "Movement")
		bool EnableTrace;


	// Called every frame
	virtual void Tick(float DeltaTime) override;


};