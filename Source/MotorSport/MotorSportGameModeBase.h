// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MotorSportGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class MOTORSPORT_API AMotorSportGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
